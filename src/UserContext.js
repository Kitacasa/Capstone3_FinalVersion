import React from 'react';

// Initialize a react context
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;
