import {Link} from 'react-router-dom'


export default function ErrorPage() {
    return (
        <div style ={{
            marginTop: '15%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        }}>
            <h1 style={{fontSize: '100px', textShadow:'3px 3px 5px gray', fontWeight:'bolder'}}>404 Error</h1>
            <h3 style={{width:'70%', textAlign: 'center'}}>Page doesn't exist or some other error occured.</h3>
            <Link style={{textDecoration:'none'}} to='/'><center><button style={{marginTop:'15px'}} className='btn-login-nav mx-auto p-1'>Home</button></center></Link>

        </div>
    )
}