import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { CgMenuCake } from "react-icons/cg"


export default function Products(){
    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/searchAll`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp = {product} />
                )
            }))
        })
    }, [])

    return (
        <>
           <h1><CgMenuCake />MENU</h1>
            {products}
        </>
    )
}

